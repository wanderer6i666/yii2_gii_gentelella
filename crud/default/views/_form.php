<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;
<?php if ($generator->enableMultiLang): ?>
use backend\models\Language;
<?php endif ?>

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <?= "<?php " ?>$form = ActiveForm::begin(); ?>
                    <div class="x_title">
                        <h2><?= "<?= " ?>Html::encode($this->title) ?></h2>

                        <div class="title_right text-right">
                            <?= "<?= " ?>Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i>', [
                                'class' => 'btn-icon-save',
                                'data-toggle' => 'tooltip',
                                'data-original-title' => $model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Сохранить'),
                            ]); ?>
                            <?= "<?php " ?>if (!$model->isNewRecord): ?>
                                <?= "<?= " ?>Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id], [
                                    'class' => 'btn-icon-save',
                                    'data-toggle' => 'tooltip',
                                    'data-original-title' => Yii::t('app', 'Удалить'),
                                    'data-confirm' =>  Yii::t('app', 'Вы уверены что хотите удалить этот элемент?'),
                                ]); ?>
                            <?= "<?php " ?>endif; ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <div role="tabpanel">
                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#tab-main" role="tab" data-toggle="tab" >
                                        <?= "<?= " ?>Yii::t('app', 'Данные') ?>
                                    </a>
                                </li>
                                <?php if ($generator->enableMultiLang): ?>
                                <?= "<?php " ?>if (Yii::$app->controller->multilingual): ?>
                                    <li role="presentation" class="">
                                        <a href="#tab-translation" role="tab" data-toggle="tab" >
                                            <?= "<?= " ?>Yii::t('app', 'Переводы') ?>
                                        </a>
                                    </li>
                                <?= "<?php " ?>endif ?>
                                <?php endif ?>
                            </ul>

                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane fade active in" id="tab-main">
                                    <br>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php foreach ($generator->getColumnNames() as $attribute) {
                                                if (in_array($attribute, $safeAttributes)) {
                                                    echo "<?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
                                                }
                                            } ?>
                                        </div>

                                        <div class="col-sm-6">
                                        </div>
                                    </div>

                                </div><!-- .tab-pane -->
                                <?php if ($generator->enableMultiLang): ?>
                                <?= "<?php " ?>if (Yii::$app->controller->multilingual): ?>
                                    <!-- Mulilang -->
                                    <div role="tabpanel" class="tab-pane fade in" id="tab-translation">
                                        <br>
                                        <!-- start accordion -->
                                        <?= "<?php " ?>foreach (Language::getBehaviorsList() as $key => $name): ?>
                                            <div class="accordion" id="lang-accordion<?= "<?= " ?>$key ?>" role="tablist" aria-multiselectable="true">
                                                <div class="panel">
                                                    <a class="panel-heading" role="tab" id="heading<?= "<?= " ?>$key ?>"<?= "<?= " ?>$key ?> data-toggle="collapse" data-parent="#lang-accordion<?= "<?= " ?>$key ?>" href="#collapse<?= "<?= " ?>$key ?>" aria-expanded="true" aria-controls="collapse<?= "<?= " ?>$key ?>">
                                                        <h4 class="panel-title"><?= "<?php " ?>Yii::t('app', 'Перевод') ?> <?= "<?= " ?>$name ?></h4>
                                                    </a>
                                                    <div id="collapse<?= "<?= " ?>$key ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?= "<?= " ?>$key ?>">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <?= "<?= " ?>$form->field($model, 'title_'.$key)->textInput(['maxlength' => true]) ?>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?= "<?php " ?>endforeach; ?>

                                    </div> <!-- .tab-pane -->
                                <?= "<?php " ?>endif ?>
                                <?php endif ?>

                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group text-right">
                            <?= "<?= " ?>Html::submitButton(
                                $model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Сохранить'),
                                ['class' => $model->isNewRecord ? 'btn save btn-success' : 'btn save btn-primary']
                            ) ?>
                        </div>
                    </div>
                <?= "<?php " ?>ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
